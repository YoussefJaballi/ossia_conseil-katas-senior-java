# Katas pour profil Senior

## Durée : 30min

## Démarche à suivre pour tous les katas

- ### TDD
- ### Tester et optimiser le plus possible

# Multi-threading

**But de l'exercice** : pouvoir stopper un thread et savoir débugguer en environnement multi-threads

1. Écrire le code d'un "StopableThread", ie. un Runnable avec une méthode stop() qui stoppe le thread
2. Tester la méthode stop()
3. Écrire un test permettant à 5 threads d'incrémenter en parallèle un compteur pour aller jusqu'à 100
4. Pourquoi le test [testDates](src/test/java/com/ossia/kata/advanced/ThreadTest.java) échoue ? Quoi corriger pour qu'il
   passe ?

# Reactive programming

**Tester des endPoints d'un controller**

Modifier la classe [ReactiveTest](src/test/java/com/ossia/kata/advanced/reactive/ReactiveTest.java) en utilisant JUnit 5
pour tester les endPoints du
controller [EmployeeController](src/main/java/com/ossia/kata/advanced/reactive/controller/EmployeeController.java):

1. Ajouter les annotations nécessaires au niveau de la
   classe [ReactiveTest](src/test/java/com/ossia/kata/advanced/reactive/ReactiveTest.java) pour charger le contexte
   Spring correctement
2. Implémenter la méthode ReactiveTest.testCreateEmployee pour tester le endPoint POST "/" de création d'employés. Pour
   cela il faut :
    1. **Uiliser un WebClient de test**
    2. **Mocker la base de données Mongo** et faire en sorte de valoriser l'id de l'Employee lors de la création d'un
       Employee
    3. Vérifier que le code de retour du endPoint est conforme à ce qu'on attend
    4. Vérifier que la valeur de l'id présent dans le body de la réponse du endPoint est conforme à ce qu'on attend
3. Implémenter la méthode ReactiveTest.testGetEmployeesByName pour tester le endPoint GET /name/{name}
4. Implémenter la méthode ReactiveTest.testGetEmployeeById pour tester le endPoint GET /{id}