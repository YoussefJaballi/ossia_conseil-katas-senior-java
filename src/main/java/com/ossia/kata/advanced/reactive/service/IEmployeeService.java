package com.ossia.kata.advanced.reactive.service;

import com.ossia.kata.advanced.reactive.dto.Employee;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IEmployeeService {
    Mono<Employee> create(Employee e);

    Mono<Employee> findById(Long id);

    Flux<Employee> findByName(String name);


}