package com.ossia.kata.advanced.reactive.service;

import com.ossia.kata.advanced.reactive.dao.EmployeeRepository;
import com.ossia.kata.advanced.reactive.dto.Employee;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class EmployeeService implements IEmployeeService {

    private final EmployeeRepository employeeRepo;

    public Mono<Employee> create(Employee e) {
        return employeeRepo.save(e);
    }

    public Mono<Employee> findById(Long id) {
        return employeeRepo.findById(id);
    }

    public Flux<Employee> findByName(String name) {
        return employeeRepo.findByName(name);
    }


}