package com.ossia.kata.advanced.reactive;

import com.ossia.kata.advanced.reactive.controller.EmployeeController;
import com.ossia.kata.advanced.reactive.dto.Employee;
import com.ossia.kata.advanced.reactive.service.EmployeeService;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Flux;

@WebFluxTest(EmployeeController.class)
public class ReactiveTest {

    @MockBean
    private EmployeeService employeeService;

    @Test
    void testCreateEmployee() {
        Employee employeeToCreate = new Employee();
        employeeToCreate.setName("John Doe");

        Mockito.when(employeeService.create(Mockito.any(Employee.class)))
                .thenAnswer(invocation -> {
                    Employee savedEmployee = invocation.getArgument(0);
                    savedEmployee.setId(42L);
                    return Mono.just(savedEmployee);
                });

        WebTestClient
            .bindToController(new EmployeeController(employeeService))
            .build()
            .post()
            .uri("/")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(employeeToCreate), Employee.class)
            .exchange()
            .expectStatus().isCreated()
            .expectBody(Employee.class)
            .value(employee -> {
                assert employee.getId() != null;
                assert employee.getName().equals("John Doe");
            });
    }

    @Test
    void testGetEmployeesByName() {
        String testName = "John Doe";
        Employee employee1 = new Employee();
        employee1.setId(1L);
        employee1.setName(testName);
        Employee employee2 = new Employee();
        employee2.setId(2L);
        employee2.setName(testName);

        Mockito.when(employeeService.findByName(testName))
                .thenReturn(Flux.just(employee1, employee2));

        WebTestClient
            .bindToController(new EmployeeController(employeeService))
            .build()
            .get()
            .uri("/name/{name}", testName)
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(Employee.class)
            .hasSize(2)
            .contains(employee1, employee2);
    }

    @Test
    void testGetEmployeeById() {
        Long testId = 1L;
        Employee employee = new Employee();
        employee.setId(testId);
        employee.setName("John Doe");

        Mockito.when(employeeService.findById(testId))
                .thenReturn(Mono.just(employee));

        WebTestClient
            .bindToController(new EmployeeController(employeeService))
            .build()
            .get()
            .uri("/{id}", testId)
            .exchange()
            .expectStatus().isOk()
            .expectBody(Employee.class)
            .isEqualTo(employee);
    }
}
