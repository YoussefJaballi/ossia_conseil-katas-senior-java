package com.ossia.kata.advanced;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadTest {

    /**
     * Why this test fails ?
     * */
    @Test
    void testDates() throws InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(5);
        Set<String> days = Collections.synchronizedSet(new HashSet<>());
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        for(int i = 0; i<31; i++) {
            GregorianCalendar cal = new GregorianCalendar(2022, 0, i);
            pool.submit(()-> days.add(sdf.format(cal.getTime())));
        }
        pool.shutdown();
        pool.awaitTermination(1, TimeUnit.SECONDS);
        Assertions.assertEquals(31, days.size());
    }
}
