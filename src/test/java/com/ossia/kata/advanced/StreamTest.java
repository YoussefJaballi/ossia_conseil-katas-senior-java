package com.ossia.kata.advanced;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

public class StreamTest {

    @Test
    public void shouldAddToMyCollection() {
        List<String> list = Stream.of("1", "2").toList();
        //Pourquoi list.add("3") throws UnsupportedOperationException ?
        Assertions.assertThrows(UnsupportedOperationException.class, () -> list.add("3"));
        Assertions.assertEquals(2, list.size());
    }

}
